Set Up:
* [x] Install Django app into Django project for hats microservice

models.py
Hat Model Planning:
* [x]  fabric attribute
  * [x]  charfield, max length 200
* [x]  style_name attribute
  * [x]  charfield, max length 200
* [x]  color attribute
  * [x]  charfield, max length 200
* [x]  picture_url attribute
  * [x]  urlField, null=True
* [x]  location attribute
  * [x]  ForeignKey, relates to LocationVO model, related_name "hats", on delete "Cascade"

LocationVO Planning: 
* We are creating a LocationVO because the Location model is in another bounded context seperate from hats. Therefore to use "Location" we would have to create another Location model in hats_rest models.py and we attach VO to the end to signify it is a value object.
* [x] close_name attribute:
  * [x] charfield, max length 100
* [x] section_number attribute:
  * [x] PositiveSmallintegerField
* [x] shelf_number attribute:
  * [x] PositiveSmallintegerField

views.py
* [ ] Import LocationVo and Hat models
* [ ] Create Encoders
  * [x] LocationVODetailEncoder
    * [x] model = LocationVO
    * [x] properties [import_href, closet_name, section_number, shelf_number]
  * [x] HatListEncoder
    * [x] model = Hat
    * [x] properties = ["fabric", "style_name", "color", "picture_url"]
    * [x] encoders = {"location": LocationVODetailEncoder()}
    * [x] def get_extra_data(self, o):
      * [x] return {
            "location": o.location.import_href,
            "closet_name": o.location.closet_name,
            "section_number": o.location.section_number,
            "shelf_number": o.location.shelf_number
            }
    * [x] HatDetailEncoder:
      * [x] model = Hat
      * [x] properties = {
        "id",
        "fabric",
        "style_name",
        "color",
        "pictur_url",
        }
      * [x] encoders = {"location": LocationDetailEncoder()}
      * [x] def get_extra_data(self, o):
      * [x] return {
            "location": o.location.import_href,
            "closet_name": o.location.closet_name,
            "section_number": o.location.section_number,
            "shelf_number":o.location.shelf_number,
            }

Hat list
* [x] require_http_methods(["GET", "POST"])
* [x] def api_list_hats(request):
* [x] GET
  * [x] if request.method == "GET":
  * [x] hats = Hat.objects.all()
  * [x] return JsonResponse(
  * [x] {"hats": hats},
  * [x] encoder=HatListEncoder)
* [ ] POST
  * [ ] else:
    * [ ] content = js.loads(request.body)
    * [ ] try
      * [ ] location_href = content["location"]
      * [ ] location = LocationVO.objects.get(import_href=location_href)
      * [ ] content["location"] = location
    * [ ] except LocationVO.DoesNotExist"
      * [ ] return JsonResponse(
        * [ ] {"messege": "invalid location id"},
        * [ ] status=400,)
    * [ ] hat = Hat.objects.create(**content)
    * [ ] return JsonResponse(
    * [ ] hat,
    * [ ] encoder=HatDeatilEncoder,
    * [ ] safe=False,)

api_urls.py

* [ ] Create URL paths for list hats and hat detail
* [ ] api_list_hats
  * [ ] path("wardrobe/<int:location_vo_id>/hats", api_list_hats, name="api_list_hats"),