from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from django.shortcuts import render

from common.json import ModelEncoder
from .models import Hat, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href", 
        "closet_name", 
        "section_number", 
        "shelf_number"
        ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name", 
        "color", 
        "picture_url", 
        ]

    def get_extra_data(self, o):
        return {
            "location": o.location.import_href,
            "closet_name": o.location.closet_name,
            "section_number": o.location.section_number,
            "shelf_number": o.location.shelf_number
        }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        'id',
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):

    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    # if request.method == "GET":
    #     hats = Hat.objects.all()
    #     return JsonResponse(
    #         hats,
    #         encoder=HatListEncoder,
    #         safe=False
    #     )

    else:
        content = json.loads(request.body)
        print(content)
        try:
            id = content['location']
            print(id)
            location = LocationVO.objects.get(id=id)
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

