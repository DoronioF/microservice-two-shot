import React, {useState} from 'react';


export default function ShoeForm({bins, getShoes}) {
    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [bin, setBin] = useState('');

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value)
    }
    const handleManufacturerChange = (e) => {
        const value = e.target.value;
        setManufacturer(value)
    }
    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value)
    }
    const handleBinChange = (e) => {
        const value = e.target.value;
        setBin(value)
    }
    const handlePictureChange = (e) => {
        const value = e.target.value;
        setPicture(value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.name = name;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = picture;
        data.bin = bin;
        console.log(data);

        const shoeUrl = `http://localhost:8080/api/shoes/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json'
            }
        }
        const response = await fetch(shoeUrl, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json();
            setName('')
            setManufacturer('')
            setColor('')
            setBin('')
            setPicture('')
            getShoes()
        }
    }
    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePictureChange} value={picture} placeholder="Picture" required type="text" name="Picture" id="Picture" className="form-control"/>
                    <label htmlFor="Picture">Picture</label>
                </div>

                <div className="mb-3">
                    <select required onChange={handleBinChange} name="bin" id="bin" className="form-select" value={bin} >
                        <option value="">Choose a Bin</option>
                        {bins.map(bin => {
                            return (
                                <option key={bin.id} value={bin.href}>
                                    {bin.closet_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>

            </div>
        </div>
    </div>
    );
}
// class ShoeForm extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             name: '',
//             manufacturer: '',
//             color: '',
//             bin: '',
//             bins: []
//         };
//         this.handleSubmit = this.handleSubmit.bind(this);
//         this.handleNameChange = this.handleNameChange.bind(this);
//         this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
//         this.handleColorChange = this.handleColorChange.bind(this);
//         this.handleBinChange = this.handleBinChange.bind(this);

//     }

//     handleNameChange(event) {
//         const value = event.target.value;
//         this.setState({name: value})
//     }

//     handleManufacturerChange(event) {
//         const value = event.target.value;
//         this.setState({manufacturer: value})
//     }

//     handleColorChange(event) {
//         const value = event.target.value;
//         this.setState({color: value})
//     }

//     handleBinChange(event) {
//         const value = event.target.value;
//         this.setState({bin: value})
//     }

//     async handleSubmit(event) {
//         event.preventDefault();
//         const data = {...this.state};
//         delete data.bins;
//         console.log(data);    
    
//         const shoesUrl = 'http://localhost:8080/api/shoes/';
//         const fetchConfig = {
//             method: "post",
//             body: JSON.stringify(data),
//             headers: {
//             'Content-Type': 'application/json',
//             },
//         };
//         const response = await fetch(shoesUrl, fetchConfig);
//         console.log("this is the response", response)
//         if (response.ok) {
//             const newShoe = await response.json();
//             console.log(newShoe);
//             const cleared = {
//                 name: '',
//                 manufacturer: '',
//                 color: '',
//                 bin: '',
//             };
//             this.setState(cleared);  
//             }
//         }

//         async componentDidMount() {
//             const url = 'http://localhost:8100/api/bins/';
        
//             const response = await fetch(url);
        
//             if (response.ok) {
//                 let data = await response.json();
//                 this.setState({bins: data.bins});
//             }
//         }

//         render() {
//             return (
//                 <div className="row">
//                 <div className="offset-3 col-6">
//                 <div className="shadow p-4 mt-4">
//                     <h1>Create a new Shoe</h1>
//                     <form onSubmit={this.handleSubmit} id="create-shoe-form">
//                     <div className="form-floating mb-3">
//                         <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
//                         <label htmlFor="name">Name</label>
//                     </div>
//                     <div className="form-floating mb-3">
//                         <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
//                         <label htmlFor="manufacturer">Manufacturer</label>
//                     </div>
//                     <div className="form-floating mb-3">
//                         <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
//                         <label htmlFor="color">Color</label>
//                     </div>
        
//                     <div className="mb-3">
//                         <select required onChange={this.handleBinChange} name="bin" id="bin" className="form-select" value={this.state.bin} >
//                             <option value="">Choose a Bin</option>
//                             {this.state.bins.map(bin => {
//                                 return (
//                                     <option key={bin.id} value={bin.href}>
//                                         {bin.closet_name}
//                                     </option>
//                                 );
//                             })}
//                         </select>
//                     </div>
//                     <button className="btn btn-primary">Create</button>
//                     </form>
        
//                 </div>
//             </div>
//         </div>
//         );
//     }
// }
        
// export default ShoeForm;
        



    