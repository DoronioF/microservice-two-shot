function HatsList({hats, getHats}) {
    const deleteHat = async (id) => {
        fetch(`http://localhost:8090/api/hats/${id}/`, {
        method: "delete"
        })
        .then(() => {
        return getHats()
        }).catch(console.log)
    }
    if(hats === undefined){
        return null;
    }    
    return (
        <table className="table table-striped table-bordered">
            <caption>List of hats</caption>
            <thead>
                <tr>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Picture</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {hats.map((hat) => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.style_name}</td>
                            <td>{hat.color}</td>
                            <td>{hat.fabric}</td>
                            <td><img src={hat.picture_url} width="50"/></td>
                            <td>{hat.closet_name}</td>
                            <td>
                            <button onClick={() => deleteHat(hat.id)} type="button" className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}


export default HatsList;