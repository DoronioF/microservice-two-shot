import React, {useState} from 'react'

export default function HatForm({locations, getHats}) {
    const [fabric, setFabric] = useState('');
    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');

    const handleFabricChange = (e) => {
        const value = e.target.value
        setFabric(value)
    }
    const handleNameChange = (e) => {
        const value = e.target.value
        setName(value)
    }
    const handleColorChange = (e) => {
        const value = e.target.value
        setColor(value)
    }
    const handlePictureChange = (e) => {
        const value = e.target.value
        setPicture(value)
    }
    const handleLocationChange = (e) => {
        const value = e.target.value
        setLocation(value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style_name = name;
        data.color = color;
        data.picture_url = picture;
        data.location = location;

        console.log(data);

        const hatUrl = `http://localhost:8090/api/hats/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json'
            }
        }
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            const newHat = await response.json();
            setName('')
            setFabric('')
            setName('')
            setColor('')
            setPicture('')
            setLocation('')
            getHats()
        }
    }
    

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric} />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Style" required type="text" name="style_name" id="style_name" className="form-control" value={name} />
                            <label htmlFor="style_name">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureChange} placeholder="picture_url" required type="url" name="picture_url" id="picture_url" className="form-control" value={picture} />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select required onChange={handleLocationChange} name="location" id="location" className="form-select" value={location}>
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

// class HatForm extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             fabric: '',
//             style_name: '',
//             color: '',
//             picture_url: '',
//             location: '',
//             locations: [],
//         }

//         this.handleSubmit = this.handleSubmit.bind(this);
//         this.handleFabricChange = this.handleFabricChange.bind(this);
//         this.handleStyleChange = this.handleStyleChange.bind(this);
//         this.handleColorChange = this.handleColorChange.bind(this);
//         this.handlePictureChange = this.handlePictureChange.bind(this);
//         this.handleLocationChange = this.handleLocationChange.bind(this);

//     }

//     handleFabricChange(event) {
//         const value = event.target.value;
//         this.setState({ fabric: value });
//     }

//     handleStyleChange(event) {
//         const value = event.target.value;
//         this.setState({ style_name: value });
//     }

//     handleColorChange(event) {
//         const value = event.target.value;
//         this.setState({ color: value });
//     }

//     handlePictureChange(event) {
//         const value = event.target.value;
//         this.setState({ picture_url: value });
//     }

//     handleLocationChange(event) {
//         const value = event.target.value;
//         this.setState({ location: value });
//     }

//     async handleSubmit(event) {
//         event.preventDefault();
//         const data = { ...this.state };
//         delete data.locations;
//         console.log(data);

//         const hatUrl = "http://localhost:8090/api/hats/";
//         const fetchConfig = {
//             method: "post",
//             body: JSON.stringify(data),
//             headers: {
//                 "Content-Type": "application/json",
//             },
//         };
//         const response = await fetch(hatUrl, fetchConfig);
//         if (response.ok) {
//             const newHat = await response.json();
//             console.log(newHat);

//             const cleared = {
//                 fabric: '',
//                 style_name: '',
//                 color: '',
//                 picture_url: '',
//                 location: '',
//             };

//             this.setState(cleared);
//         }
//     }

//     async componentDidMount() {
//         const url = "http://localhost:8100/api/locations/";
//         const response = await fetch(url);
//         if (response.ok) {
//             let data = await response.json();
//             this.setState({ locations: data.locations })
//         }
//     }

//     render() {
//         return (
//             <div className="row">
//                 <div className="offset-3 col-6">
//                     <div className="shadow p-4 mt-4">
//                         <h1>Create a new hat</h1>
//                         <form onSubmit={this.handleSubmit} id="create-hat-form">
//                             <div className="form-floating mb-3">
//                                 <input onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={this.state.fabric} />
//                                 <label htmlFor="fabric">Fabric</label>
//                             </div>
//                             <div className="form-floating mb-3">
//                                 <input onChange={this.handleStyleChange} placeholder="Style" required type="text" name="style_name" id="style_name" className="form-control" value={this.state.style_name} />
//                                 <label htmlFor="style_name">Style</label>
//                             </div>
//                             <div className="form-floating mb-3">
//                                 <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={this.state.color} />
//                                 <label htmlFor="color">Color</label>
//                             </div>
//                             <div className="form-floating mb-3">
//                                 <input onChange={this.handlePictureChange} placeholder="picture_url" required type="url" name="picture_url" id="picture_url" className="form-control" value={this.state.picture_url} />
//                                 <label htmlFor="picture_url">Picture URL</label>
//                             </div>
//                             <div className="mb-3">
//                                 <select required onChange={this.handleLocationChange} name="location" id="location" className="form-select" value={this.state.location}>
//                                     <option value="">Choose a location</option>
//                                     {this.state.locations.map(location => {
//                                         return (
//                                             <option key={location.id} value={location.id}>
//                                                 {location.closet_name}
//                                             </option>
//                                         )
//                                     })}
//                                 </select>
//                             </div>
//                             <button className="btn btn-primary">Create</button>
//                         </form>
//                     </div>
//                 </div>
//             </div>
//         )
//     }
// }

// export default HatForm;